//
//  ViewController.swift
//  WeatherAppFull
//
//  Created by Администратор on 28.05.2020.
//  Copyright © 2020 Areas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tempHoursView: UIView!
    
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var descLabel: UILabel!
  
    @IBOutlet weak var tempLabel: UILabel!
    
    
    var cityName = ""
    var cityTemp = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(cityName)
        
        tempHoursView.layer.borderWidth = 1
        tempHoursView.layer.borderColor = UIColor.white.cgColor
        cityLabel.text = cityName
        tempLabel.text = cityTemp + " C"
    }


}

