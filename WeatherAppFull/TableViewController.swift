//
//  TableViewController.swift
//  WeatherAppFull
//
//  Created by Администратор on 28.05.2020.
//  Copyright © 2020 Areas. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class TableViewController: UITableViewController {
    
    struct City {
        var name = ""
        var temperature = ""
        var time = ""
    }
    
    var citiesArray = [City]()

    var cityName = ""
    var cityTemp = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.view.addSubview(<#T##view: UIView##UIView#>)
//        self.view.insertSubview(<#T##view: UIView##UIView#>, at: <#T##Int#>)
//        self.view.layer.addSublayer(<#T##layer: CALayer##CALayer#>)
//        let button = UIButton(frame: CGRect(x: 100, y: 100, width: 300, height: 50))
//        button.backgroundColor = .black
//        self.view.addSubview(button)
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return citiesArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellCity", for: indexPath) as! TableViewCell
        cell.cityLabel.text = citiesArray[indexPath.row].name
        cell.tempLabel.text = citiesArray[indexPath.row].temperature
        cell.timeLable.text = citiesArray[indexPath.row].time
        // Configure the cell...
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        cityName = citiesArray[indexPath.row].name
        performSegue(withIdentifier: "toDetail", sender: self)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    // Функция получения текущей погоды
    func getCurrentWeather(city: String) {
        let url = "https://api.openweathermap.org/data/2.5/weather?q=\(city)&units=metric&apiKey=6da8e8bdb22e1d408ffb437eab399b45"
        Alamofire.request(url, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                print("JSON: \(json)")
                
                let temp = json["main"]["temp"].stringValue
                self.citiesArray.append(City(name: city, temperature: temp, time: ""))
                self.tableView.reloadData()
                self.cityTemp = temp
            case .failure(let error):
                print(error)
                let errorAlert = UIAlertController(title: "Ошибка", message: "Вы ввели неправильно город.", preferredStyle: .alert)
                let actionClose = UIAlertAction(title: "Закрыть", style: .cancel, handler: nil)
                errorAlert.addAction(actionClose)
                self.present(errorAlert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func addCity(_ sender: Any) {
        let alertController = UIAlertController(title: "Add city", message: "", preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.placeholder = "Moscow"
        }
        
        let actionOK = UIAlertAction(title: "OK", style: .default) { (_) in
            let cityNameFromTextField = alertController.textFields![0].text
            self.getCurrentWeather(city: cityNameFromTextField!)
        }
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(actionOK)
        alertController.addAction(actionCancel)
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDetail" {
            if let detailViewController = segue.destination as? ViewController {
                detailViewController.cityName = self.cityName
                detailViewController.cityTemp = self.cityTemp
            }
        }
    }

}
